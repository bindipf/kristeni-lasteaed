import React from 'react';
import { shallow } from 'enzyme';
import assert from 'assert';

import { UserListElement } from '../../src/components/UserListElement';

describe('UserListElement component', () => {
    describe('render()', () => {
        it('should render the component', () => {
            const props = {
                user: {
                    id: 1,
                    username: 'John',
                    job: 'CEO',
                }
            };
            const wrapper = shallow(<UserListElement {...props} />);
            assert.equal(wrapper.find('td').length, 5);
            assert.equal(wrapper.find('Button').length, 2);
        });
    });
});