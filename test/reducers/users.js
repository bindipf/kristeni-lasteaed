import assert from 'assert';

import users from '../../src/reducers/users';

// unit tests for the users reducer
describe('Users reducer', () => {
    describe('add()', () => {
        it('should return a new user array element', () => {
            const state = {
                list: [
                    {
                        id: 1,
                        username: 'Some name',
                        job: 'Some job',
                    }
                ]
            };
            const action = {
                type: 'users.add',
                id: 2,
                username: 'Other name',
                job: 'Other job',
            };
            const expected = {
                list: [
                    {
                        id: 1,
                        username: 'Some name',
                        job: 'Some job',
                    },
                    {
                        id: 2,
                        username: 'Other name',
                        job: 'Other job',
                    }
                ]
            };
            assert.deepEqual(users(state, action), expected);
        });
    });

    describe('edit()', () => {
        it('should return an edited user array element', () => {
            const state = {
                list: [
                    {
                        id: 1,
                        username: 'Some name',
                        job: 'Some job',
                    },
                    {
                        id: 2,
                        username: 'Other name',
                        job: 'Other job',
                    }
                ],
            };
            const action = {
                type: 'users.edit',
                id: 2,
                username: 'Changed name',
                job: 'Changed job',
            };
            const expected = {
                list: [
                    {
                        id: 1,
                        username: 'Some name',
                        job: 'Some job',
                    },
                    {
                        id: 2,
                        username: 'Changed name',
                        job: 'Changed job',
                    }
                ],
            };
            assert.deepEqual(users(state, action), expected);
        });
    });

    describe('modalDeleteShow()', () => {
        it('should set the list_delete data when its undefined', () => {
            const state = {};
            const action = {
                type: 'users.modalDeleteShow',
                id: 2,
                username: 'John',
            };
            const expected = {
                modal: {
                    list_delete: {
                        show: true,
                        id: 2,
                        username: 'John',
                    }
                }
            };
            assert.deepEqual(users(state, action), expected);
        });
        it('should set the list_delete data when it exists', () => {
            const state = {
                modal: {
                    list_delete: {
                        show: false,
                        id: 0,
                        username: '',
                    }
                }
            };
            const action = {
                type: 'users.modalDeleteShow',
                id: 2,
                username: 'John',
            };
            const expected = {
                modal: {
                    list_delete: {
                        show: true,
                        id: 2,
                        username: 'John',
                    }
                }
            };
            assert.deepEqual(users(state, action), expected);
        });
    });

    describe('modalDeleteHide()', () => {
        it('should set the list_delete data', () => {
            const state = {
                modal: {
                    list_delete: {
                        show: true,
                        id: 2,
                        username: 'John',
                    }
                }
            };
            const action = {
                type: 'users.modalDeleteHide',
            };
            const expected = {
                modal: {
                    list_delete: {
                        show: false,
                        id: 0,
                        username: '',
                    }
                }
            };
            assert.deepEqual(users(state, action), expected);
        });
    });

    describe('delete()', () => {
        it('should return the user array without the deleted element', () => {
            const state = {
                list: [
                    {
                        id: 1,
                        username: 'Some name',
                        job: 'Some job',
                    }, {
                        id: 2,
                        username: 'Other name',
                        job: 'Other job',
                    }
                ]
            };
            const action = {
                type: 'users.delete',
                id: 2,
            };
            const expected = {
                list: [
                    {
                        id: 1,
                        username: 'Some name',
                        job: 'Some job',
                    }
                ]
            };
            assert.deepEqual(users(state, action), expected);
        });
    });

    describe('fetchListSuccess()', () => {
        it('should return a list of users', () => {
            const state = {};
            const action = {
                type: 'users.fetchListSuccess',
                users: [{
                    id: 1,
                    username: 'Some name',
                    job: 'Some job',
                }],
            };
            const expected = {
                list: [{
                    id: 1,
                    username: 'Some name',
                    job: 'Some job',
                }],
            };
            assert.deepEqual(users(state, action), expected);
        });
    });
});