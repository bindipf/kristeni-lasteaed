import { takeLatest } from 'redux-saga';
import { fork } from 'redux-saga/effects';

import { usersFetchList, usersAdd, usersEdit, usersDelete } from './users';

export function* sagas() {
    yield [
        fork(takeLatest, 'usersFetchList', usersFetchList),
        fork(takeLatest, 'usersAdd', usersFetchList),
        fork(takeLatest, 'usersEdit', usersFetchList),
        fork(takeLatest, 'usersDelete', usersFetchList),
    ];
}