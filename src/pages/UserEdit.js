import React from 'react';

import { PageHeader, Form, FormGroup, Col, Button, FormControl, InputGroup, Glyphicon, HelpBlock } from 'react-bootstrap';
import { Field, reduxForm } from 'redux-form';
import { connect } from 'react-redux';
import { goBack } from 'react-router-redux';

import UserEditUsername from '../components/UserEditUsername';
import UserEditJob from '../components/UserEditJob';

export class UserEdit extends React.Component {
    form_type;

    constructor (props) {
        super(props)
        this.form_type = (props.initialValues.id > 0) ? 'edit' : 'add';
        this.formSubmit = this.formSubmit.bind(this);
    }
    

    render() {
        return (
            <div>
                <PageHeader>
                    {'edit' === this.form_type ? 'User edit' : 'User add'}
                </PageHeader>
                <Form horizontal onSubmit={this.props.handleSubmit(this.formSubmit)}>
                    <Field name="username" component={UserEditUsername} />
                    <Field name="job" component={UserEditJob} />
                    <FormGroup>
                        <Col smOffset={2} sm={8}>
                            <Button disabled={this.props.invalid || this.props.submitting} type="submit">Save User</Button>
                        </Col>
                    </FormGroup>
                </Form>
            </div>
        );
    }

    formSubmit(values) {
        const upper_form_type = this.form_type.charAt(0).toUpperCase() + this.form_type.slice(1);
        this.props.dispatch({
            type: 'users' + upper_form_type,
            id: values.id,
            username: values.username,
            job: values.job,
        });
        
        this.props.dispatch({
            type: 'users.' + this.form_type,
            id: values.id,
            username: values.username,
            job: values.job,
        });

        this.props.dispatch(goBack());
    }
}

const UserEditForm = reduxForm({
    form: 'user_edit',
    validate: function (values) {
        const errors = {};
        if (!values.username) {
            errors.username = 'Username is required';
        }
        return errors;
    },
})(UserEdit);

function mapStateToProps(state, own_props) {
    let form_data = {
        id: 0,
        username: '',
        job: '',
    }
    for (const user of state.users.list) {
        if (user.id === Number(own_props.params.id)) {
            form_data = user;
            break;
        }
    }

    return {
        initialValues: form_data,
    }
}

export default connect(mapStateToProps)(UserEditForm);